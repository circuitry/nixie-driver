#define BCD1_A D1
#define BCD1_B D3
#define BCD1_C D4
#define BCD1_D D2

#define BCD2_A D5
#define BCD2_B D6
#define BCD2_C D0
#define BCD2_D D7


void setup() {
  pinMode(BCD1_A, OUTPUT);
  pinMode(BCD1_B, OUTPUT);
  pinMode(BCD1_C, OUTPUT);
  pinMode(BCD1_D, OUTPUT);
  
  pinMode(BCD2_A, OUTPUT);
  pinMode(BCD2_B, OUTPUT);
  pinMode(BCD2_C, OUTPUT);
  pinMode(BCD2_D, OUTPUT);
}

void nixieDisplay(byte n1, byte n2) {
  digitalWrite(BCD1_A, n1 & 1);
  digitalWrite(BCD1_B, n1 & 2);
  digitalWrite(BCD1_C, n1 & 4);
  digitalWrite(BCD1_D, n1 & 8);

  digitalWrite(BCD2_A, n2 & 1);
  digitalWrite(BCD2_B, n2 & 2);
  digitalWrite(BCD2_C, n2 & 4);
  digitalWrite(BCD2_D, n2 & 8);
}

long number = 0;

void loop() {
  number++;
  number = number % 100;
  nixieDisplay(number % 10, number / 10);
  delay(250);
}
